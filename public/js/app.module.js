angular.module('app', ['ngResource']);

angular.module('app').factory('ProductFactory', [
    '$resource',
    function ($resource) {
        return $resource('/products/');
    }
]);