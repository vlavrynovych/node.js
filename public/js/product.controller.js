angular.module('app').controller('ProductController', [
    '$scope',
    'ProductFactory',
    function ($scope, Product) {
        refreshList();

        $scope.save = function () {
            Product.save($scope.product, function() {
                refreshList();
                $scope.product = {}
            });
        };

        $scope.test = function () {
            return $scope.product.name + $scope.product.price
        };

        function refreshList() {
            $scope.products = Product.query();
        }
    }
]);