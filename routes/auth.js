var express = require('express'),
    passport = require('passport'),
    User = require('../models/user'),
    LocalStrategy = require('passport-local').Strategy
    router = express.Router();

router.post('/registration', function(req, res, next) {
    User.create(req.body, function (err, user) {
        if(err) {
            return next(err)
        }
        res.send(user);
    });
});

passport.use(new LocalStrategy(
    function (username, password, done) {
        User.findOne({
            username: username,
            password: password
        }, '-password', function(err, user){
            if (err) {
                return done(err);
            }
            if (!user) {
                return done(null, false, { message: 'Incorrect username or password.' });
            }
            return done(null, user);
        });
    }
));

passport.serializeUser(function (user, done) {
    done(null, user && user._id);
});

passport.deserializeUser(function (id, done) {
    User.findOne({
        _id: id
    }, '-password', function(err, user){
        if(err) {
            console.error(err);
            return done(err);
        }
        done(null, user);
    });
});

router.post('/login', function(req, res, next) {
    passport.authenticate('local', function(err, user, info) {
        if (err) return next(err); // will generate a 500 error

        // Generate a JSON response reflecting authentication status
        if (!user) {
            return res.json({
                success: false,
                data: {
                    message: 'Authentication failed',
                    details: info.message
                }
            });
        }

        req.logIn(user, function(err) {
            if (err) return next(err);

            res.json({
                success: true,
                data: {
                    message: 'Authentication succeeded',
                    user: user
                }
            });
        });
    })(req, res, next);
});

router.get('/logout', function(req, res){
    req.logout();
    res.json({ success: true });
});

router.get('/info', function (req, res) {
    if(req.user) {
        res.json({
            success: true,
            data: req.user
        });
    } else res.send(401);
});

module.exports = router;